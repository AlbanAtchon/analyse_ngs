#!/bin/bash
#SBATCH --time=2:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=3G
##SBATCH --mem=12G
#SBATCH -o log/slurmjob-%j
#SBATCH --job-name=qc_trim
#SBATCH --partition=short
#SBATCH --array=0-7

# Program configuration
__author__='Nadia Goué'
__email__='nadia.goue@uca.fr'
__credits__=["Nadia Goué"]
__license__='GPL3'
__maintainer__='Nadia Goué'
__status__='Development'
__version__='0.0.1'

echo 'Quality control and trimming'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose
#set -euo pipefail

IFS=$'\n\t'

#Set up whatever package we need to run with
module load java/oracle-1.7.0_79 fastqc/0.11.7 trimmomatic/0.38


echo "Set up directories ..." >&2
#Set up the temporary directory
SCRATCHDIR=/storage/scratch/"$USER"/"$SLURM_JOB_ID"

OUTPUT="$HOME"/results/chipseq/qc_trim
mkdir -p "$OUTPUT"
mkdir -p -m 700 "$SCRATCHDIR"
cd "$SCRATCHDIR"

#Set up data directory
DATA_DIR="/home/users/shared/data/set"

#Set up results sub-directories
mkdir -p "$SCRATCHDIR/fastqc-init"
mkdir -p "$SCRATCHDIR/fastqc-post"

#Run the program
echo "Start on $SLURMD_NODENAME: "`date` >&2

echo "Set up an array with all fastq.gz sequence files..." >&2
tab=($(ls "$DATA_DIR"/*.fastq.gz))
echo "tab = " >&2
printf '%s\n' "${tab[@]}" >&2

echo "Run the First quality control... " >&2
fastqc "${tab[$SLURM_ARRAY_TASK_ID]}" -dir $SCRATCHDIR -o "$SCRATCHDIR"/fastqc-init

echo "Run the trimming with trimmomamic tool..." >&2
java -jar /opt/apps/trimmomatic-0.38/trimmomatic-0.38.jar SE -threads 6 -phred33 "${tab[$SLURM_ARRAY_TASK_ID]}" $(basename "${tab[$SLURM_ARRAY_TASK_ID]}" .fastq.gz )_trim.fastq.gz SLIDINGWINDOW:4:20

echo "Run the second quality control..." >&2
fastqc $(basename "${tab[$SLURM_ARRAY_TASK_ID]}" .fastq.gz )_trim.fastq.gz -dir $SCRATCHDIR -o "$SCRATCHDIR"/fastqc-post

#Move results in one's directory
mv  "$SCRATCHDIR" "$OUTPUT"

echo "Stop job : "`date` >&2



