#! /bin/bash
#cd "$(dirname "$0")"

echo 'GOUE Nadia (Universite Clermont Auvergne, Mesocentre)'
echo 'Date: Fall Master course 2019 '
echo 'Object: Sample case of CHIPseq workflow showing job execution and dependency handling.'
echo 'Inputs: paths to scripts qc_trim and bwa'
echo 'Outputs: trimmed fastq files, QC HTML files and BAM files'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose

IFS=$'\n\t'

# first job - no dependencies
# QC and Trim
jid1=$(sbatch --parsable scripts/qc_trim.slurm)

echo "$jid1 : qc_trim"

##TODO: multiqc

# multiple jobs can depend on a single job
# Alignment and mapping
jid2=$(sbatch --parsable --dependency=afterok:$jid1 scripts/bwa_array.slurm)

echo "$jid2 : bwa"

# show dependencies in squeue output:
squeue -u $USER -o "%.8A %.4C %.10m %.20E"
